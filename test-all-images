#!/bin/bash

# This script will build and test all images

top_dir=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)

tmp_dir=$(mktemp -d)
out_file=${tmp_dir}/output

check_result() {
  local result="$1"
  if [[ "$result" != "0" ]]; then
    echo "Fatal error encountered.  Tests aborted (exit code: ${result})"
    if [ -f ${out_file} ]; then
      cat ${out_file}
    fi
    rm -rf ${tmp_dir}
    exit $result
  fi
}

for dockerfile in ${top_dir}/Dockerfile-*; do
  file_name=$(basename ${dockerfile})
  final_tag=$(echo ${file_name} | cut -f2- -d-)
  candidate_tag="${final_tag}-build-candidate"

  echo
  echo "Building duke-ruby:${candidate_tag}"
  docker build -f ${dockerfile} -t duke-ruby:${candidate_tag} --pull ${top_dir} > ${out_file} 2>&1
  check_result $?

  echo "Starting build candidate tests..."
  rm -f ${out_file}
  ${top_dir}/test-candidate-image duke-ruby:${candidate_tag}
done

rm -rf ${tmp_dir}
